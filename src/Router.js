//dependencies
import React from "react";
import {Router, Switch, Route } from "react-router-dom";

//components
import Login from './containers/Login'
import Home from './containers/Home'
import Register from "./containers/Register";
import history from "./helper/history";
import AboutUs from "./containers/AboutUs";
import Restaurants from "./containers/Restaurants";
import Dishes from "./containers/Dishes";

const MainRouter = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={Login}/>
        <Route path="/register" component={Register}/>
        <Route path="/home" component={Home}/>
        <Route path="/about" component={AboutUs}/>
        <Route path="/restaurants" component={Restaurants}/>
        <Route path="/dishes" component={Dishes}/>
      </Switch>
    </Router>
  )
}

export default MainRouter;
 