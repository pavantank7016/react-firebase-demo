import firebase from 'firebase';

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
  apiKey: "AIzaSyAqEJUFkceE2Kvg0mossx2BNeBpVP3GbmU",
  authDomain: "react-firebase-demo-17bda.firebaseapp.com",
  projectId: "react-firebase-demo-17bda",
  storageBucket: "react-firebase-demo-17bda.appspot.com",
  messagingSenderId: "354863629443",
  appId: "1:354863629443:web:1fc8cada972c98fcf707b6",
};

// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
const database = firebase.firestore();
export const auth = firebase.auth();

export default database;