import { auth } from "../firebase"

export const register = (userData) => {
  return new Promise((resolve, reject) => {
    auth.createUserWithEmailAndPassword(userData.email, userData.password)
      .then(res => {
        console.log(res)
        resolve()
      })
      .catch(err => {
        console.log('ERROR======>',err)
        reject(err.message)
      })
  })
}

export const login = (userData) => {

  return new Promise((resolve, reject) => {
    auth.signInWithEmailAndPassword(userData.email, userData.password)
      .then(res => {
        console.log(res)
        resolve()
      })
      .catch(err => {
        console.log('ERROR======>', err)
        reject(err.message)
      })
  })
}