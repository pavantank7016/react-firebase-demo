//check if email contains all neccessary elements
const EMAIL_REGEX = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

// check if password has minimum 6 characters
const PASSWORD_REGEX = /^.{6,}$/;

export const validateEmail = email => EMAIL_REGEX.test(email);
export const validatePassword = password => PASSWORD_REGEX.test(password);