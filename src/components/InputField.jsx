//dependencies
import React from 'react';

//styles & icons
import styles from './InputField.module.scss';
import { AiFillEyeInvisible, AiFillEye } from 'react-icons/ai'
import { MdEmail, MdLock } from 'react-icons/md'

export default (props) => {
  const { placeholder, leftIconMode, rightIconMode, customStyles, onChange, value, name } = props;

  const localIcons = {
    "email": <MdEmail className={styles.icon} />,
    "password": <MdLock className={styles.icon} />,
    "eye": <AiFillEye className={styles.icon} />
  }

  return (
    <div className={styles.main} style={{ ...customStyles }}>
      <div className={styles.container}>
        {leftIconMode && localIcons[leftIconMode]}
        <div className={styles.inputSection}>
          <input
            name={name}
            className={styles.input}
            placeholder={placeholder}
            onChange={onChange}
            value={value}
          />
        </div>
      </div>
      {rightIconMode && localIcons[rightIconMode]}
    </div>
  )
}