//dependencies
import React from 'react';
import { Button, Spinner } from 'reactstrap';

export default (props) => {
  const {onClick, title, loading, style} = props;

  return (
    <Button className={style} onClick={onClick}>
      {loading ? (
        <Spinner size="sm" color="#fff" />
      ) : title}
    </Button>
  )
}
