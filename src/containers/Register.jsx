//dependencies
import React, { useState } from 'react';
import { Alert } from 'reactstrap';
import { Link } from 'react-router-dom';

//styles, icons, images & files
import BgImage from '../assets/images/bg.jpg'
import InputField from '../components/InputField';
import Button from '../components/Button';
import styles from './Login.module.scss';
import { login, register } from '../auth/firebaseAuth';
import { validateEmail, validatePassword } from '../helper/validations';
import history from '../helper/history';

export default () => {
  const [isLoading, setIsLoading] = useState(false)
  const [errorText, setErrorText] = useState("")
  const [successText, setSuccessText] = useState("")
  const [fields, setFields] = useState({
    email: '',
    password: ''
  })

  const onRegister = () => {
    if (!validateEmail(fields.email)) {
      setErrorText("Please enter valid Email Address")
    } else if (!validatePassword(fields.password)) {
      setErrorText("Password must contain minimum 6 characters")
    } else {
      setIsLoading(true);
      register(fields)
        .then(res => {
          setErrorText('')
          setSuccessText('User Registered Successfully')
          setTimeout(() => {
            setIsLoading(false)
            history.push('./home');
          }, 1500);
        })
        .catch((err => {
          setErrorText(err)
          setIsLoading(false);
        }))
    }
  }

  const updateFields = ({ target: { name, value } }) => {
    setFields({ ...fields, [name]: value })
  }

  return (
    <div className={styles.main}>
      <img src={BgImage} className={styles.bgImage} />

      <div className={styles.centerBox}>
        <p className={styles.title}>Restro Club</p>
        <p className={styles.subTitle}>Register</p>

        <InputField
          name="email"
          placeholder="Email Address"
          leftIconMode="email"
          onChange={updateFields}
          value={fields.email}
        />

        <InputField
          name="password"
          placeholder="Password"
          leftIconMode="password"
          rightIconMode="eye"
          onChange={updateFields}
          value={fields.password}
        />

        {errorText && <Alert color="danger">{errorText}</Alert>}
        {successText && <Alert color="success">{successText}</Alert>}

        <div className={styles.bottomSection}>
          <Button title="LOGIN" style={styles.button} onClick={onRegister} loading={isLoading} />
        </div>

        <div className={styles.note}>
          <p>Already have an Account ? <Link to="/" >Login</Link></p>
        </div>
      </div>
    </div>
  )
};