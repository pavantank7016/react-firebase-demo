//dependencies
import React from 'react';
import { Link } from 'react-router-dom';

//files
import styles from './Home.module.scss';
import Section1Cover from '../assets/images/food_cover.jpg'
import Section2Cover from '../assets/images/restro_cover.jpg'
import Section4Cover from '../assets/images/burger.png'
import Section5Image from '../assets/images/user.png'
import RestroIcon from '../assets/icons/restro.png'
import FoodIcon from '../assets/icons/food.png'
import RatingIcon from '../assets/icons/rating.png'
import DishIcon from '../assets/icons/dishes.png'

export default () => {
  return (
    <div className={styles.container}>

      <div className={styles.section1}>
        <img src={Section1Cover} alt="404! food image" />
        <div className={styles.sectionBody}>
          <div className={styles.header}>
            <Link className={styles.navs} to="/home">Home</Link>
            <Link className={styles.navs} to="/about">About Us</Link>
            <Link className={styles.navs} to="/restaurants">Restaurants</Link>
            <Link className={styles.navs} to="dishes">Dishes</Link>
            <Link className={styles.navs} to="/">Log out</Link>
          </div>
          <div className={styles.coverDetails}>
            <h1>Welcome to <br /> Restro Hub</h1>
            <h3>Find Nearby Restuarants and Dishes</h3>
          </div>
        </div>
      </div>

      <div className={styles.section2}>
        <div className={styles.parts}>
          <img src={RestroIcon} className={styles.icon} />
          <p>Find Nearby <br /> Restaurants</p>
        </div>
        <div className={styles.parts}>
          <img src={RatingIcon} className={styles.icon} />
          <p>Restaurant <br /> Reviews & Ratings </p>
        </div>
        <div className={styles.parts}>
          <img src={FoodIcon} className={styles.icon} />
          <p>Restaurant's <br /> Speciality</p>
        </div>
        <div className={styles.parts}>
          <img src={DishIcon} className={styles.icon} />
          <p>Explore Tasty <br /> Dishes</p>
        </div>
      </div>

      <div className={styles.section3}>
        <img src={Section2Cover} alt="404! Restro image" />
        <div className={styles.sectionBody}>
          <h1>Find Nearby Restaurants</h1>
          <p>We have list of Restaurants nearby you and we also provide some of the Restaurant Details as well as Customer Ratings on Restaurants.</p>
          <div className={styles.button}>Explore</div>
        </div>
      </div>

      <div className={styles.section4}>
        <div className={styles.sectionBody}>
          <h1>Explore New Tasty Dishes</h1>
          <p>
            We have list of Tasty New Dishes which will satisfy your Tummy. We also provide you
            category wise Dishes like Healthy Dishes, Italian Food, Maxican Food, Chinese Food etc..
            Go to Dishes page and Explore all our Dishes via pressing below button.
            </p>
          <div className={styles.button}>Explore</div>
        </div>
        <img src={Section4Cover} alt="404! dishes image" />
      </div>

      <div className={styles.section5}>
        <span>Testimonials - Customer Say</span>
        <img src={Section5Image} alt="404! user image" />
        <span className={styles.name}>Alexandra Daddario</span>
        <span className={styles.occupation}>Artist</span>
        <span className={styles.notes}>
          I'm a Artist and also a Food lover, I've travel across the world and love to taste Regional special Food. When I came to know about this
          Application It become more Easier to find Near by Restaurants and well known Tasty Dishes for me. It's very helpful Application for
          a person like me and loves to explore new Dishes and Restaurants.
        </span>
      </div>

      <div className={styles.section6}>
        2021&copy; Restro Hub | Designed by Vaidehi Khengar.
        Please reach us to contact on <a href="mailto:vaidehikhengar5@gmail.com">vaidehikhengar5@gmail.com</a>
      </div>
    </div>
  )
}