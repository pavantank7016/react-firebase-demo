//dependencies
import React from 'react';
import { Link } from 'react-router-dom';

//files
import styles from './AboutUs.module.scss'
import Section1Cover from '../assets/images/food_cover.jpg'


export default () => {
  return (
    <div className={styles.container}>

      <div className={styles.section1}>
        <img src={Section1Cover} alt="404! food image" />
        <div className={styles.sectionBody}>
          <div className={styles.header}>
            <Link className={styles.navs} to="/home">Home</Link>
            <Link className={styles.navs} to="/about">About Us</Link>
            <Link className={styles.navs} to="/restaurants">Restaurants</Link>
            <Link className={styles.navs} to="dishes">Dishes</Link>
            <Link className={styles.navs} to="/">Log out</Link>
          </div>
          <div className={styles.coverDetails}>
            <div>
              <h1>About Us <br /> Restro Hub</h1>
              <h3>Find Nearby Restuarants and Dishes</h3>
            </div>
            <div className={styles.notes}>
              <h2>Who are we ?</h2>
              <hr color="lightgray"/>
              <p>Launched in 2021, Our technology platform connects customers, restaurant partners and
              delivery partners, serving their multiple needs. Customers use our platform to search
              and discover restaurants, read and write customer generated reviews and view and upload
              photos, order food delivery, book a table and make payments while dining-out at restaurants.
              On the other hand, we provide restaurant partners with industry-specific marketing tools
              which enable them to engage and acquire customers to grow their business while also
              providing a reliable and efficient last mile delivery service. We also operate a
              one-stop procurement solution, Hyperpure, which supplies high quality ingredients
              and kitchen products to restaurant partners. We also provide our delivery partners
              with transparent and flexible earning opportunities.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}