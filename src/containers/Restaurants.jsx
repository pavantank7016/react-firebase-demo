//dependencies
import React from 'react';
import { Link } from 'react-router-dom';

//files
import styles from './Restaurants.module.scss'
import Section1Cover from '../assets/images/food_cover.jpg'

const RESTAURANTS = [
  
]

export default () => {
  return (
    <div className={styles.container}>

      <div className={styles.section1}>
        <img src={Section1Cover} alt="404! food image" />
        <div className={styles.sectionBody}>
          <div className={styles.header}>
            <Link className={styles.navs} to="/home">Home</Link>
            <Link className={styles.navs} to="/about">About Us</Link>
            <Link className={styles.navs} to="/restaurants">Restaurants</Link>
            <Link className={styles.navs} to="dishes">Dishes</Link>
            <Link className={styles.navs} to="/">Log out</Link>
          </div>
          <div className={styles.coverDetails}>
            <h4>List Of Restaurants</h4>
          </div>
        </div>
      </div>
    </div>
  )
}